Medical Wave is a medical device company specializing in the worlds most effective EPAT and Shockwave Technology reporting a 91% success rate according to clinical studies.

EPAT also known as Extracorporeal Shock Wave Technology (ESWT) utilizes high energy acoustic pressure waves to non-invasively treat patients.

Medical Wave provides STORZ Medical Devices, the worlds most advanced EPAT/Shockwave technology to leading healthcare professionals specializing in a wide variety of medical disciplines including:

Chiropractic
Podiatry
Orthopedics
Pain Management
Regenerative Medicine
Physical Therapy and many others.

Website: https://medicalwaveus.com
